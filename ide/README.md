# 統合開発環境

VS Code + Docker + Remote Development で構築

参考 : https://code.visualstudio.com/docs/remote/remote-overview

## 作成

`root` ユーザじゃないと色々面倒そう

```
mkdir module
touch module/mod1.py

docker run \
  -it \
  --rm \
  -v python_global:/usr/local \
  -v python_user:/root/.local \
  -v $(pwd):/root/wkspc/ \
  -w /root/wkspc \
  python:3 \
    bash
```
