# selenium を使った自動 Web 操作

- https://www.selenium.dev/documentation/ja/
- https://www.selenium.dev/documentation/ja/remote_webdriver/remote_webdriver_client/
- https://github.com/SeleniumHQ/docker-selenium で起動してるサーバを操作

```
# リモートサーバ
docker run --name selenium -d -p 4444:4444 -v /dev/shm:/dev/shm selenium/standalone-firefox:4.0.0-rc-1-prerelease-20210618


docker build -t my-python-app --force-rm=true .

docker run -it --rm --name my-running-app --link selenium my-python-app

docker run \
  -it \
  --rm \
  --name my-running-app \
  --link selenium \
  -v $(pwd):/tmp/ \
  my-python-app python


docker rmi my-python-app
```
