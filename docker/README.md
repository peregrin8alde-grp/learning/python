# Docker を利用した開発

https://hub.docker.com/_/python

## アプリイメージを作成して実行

```
cd samples/01

docker build -t my-python-app --force-rm=true .

docker run -it --rm --name my-running-app my-python-app

docker rmi my-python-app
```

## スクリプト単体実行

```
docker run -it --rm --name my-running-script -v "$PWD":/usr/src/myapp -w /usr/src/myapp python:3 python your-daemon-or-script.py
```
